#!/usr/bin/env python3

import os, sys

# disable some messages from TF
os.environ['TF_CPP_MAX_VLOG_LEVEL'] = '-1'

import tensorflow as tf
import numpy      as np
import cv2

from sklearn.model_selection import train_test_split
from time                    import time

# path with images
dataset_path = "Dataset"

# path for generated model
model_path   = "Model/test.h5"

# verbosity level
verbosity    = 1

#
# read and resize all images to same size
#
def img_preprocessing ( basepath ) :
    #
    # Goes through folder, takes label by folder name
    #

    counter = 1
    images  = []
    labels  = []
    for subdir in os.listdir( basepath ) : 
        path = os.path.join( basepath, subdir )
        for filename in os.listdir( path ) : 
            if filename.endswith( ".jpg" ) :
                if verbosity >= 1 :
                    print( '\r                                           \r', end = '' )
                    print( "%05d : %s" % ( counter, filename ), end = '', flush = True )
                    counter += 1
                imgname = os.path.join( path, filename )
                img     = cv2.imread( imgname )
                img     = cv2.resize( img, ( 128, 128 ) )  # rescale to 128x128
                img     = img / 255.0                      # convert from uint8 to float
                
                images.append( img )
                labels.append( subdir )
                
    if verbosity >= 1 :
        print( '\r                                           \r', end = '' )

    return images, labels

#
# convert string label to number label
#
def generate_labels ( labels ) :
    current_label = "" 
    old_label     = "" # last label
    current_num   = -1

    new_labels  = [0] * len(labels)
    lbl_classes = []
    
    for i in range( len( labels ) ) :
        current_label = labels[i]
        
        if old_label != current_label : 
            current_num += 1
            lbl_classes.append( current_label )
        
        new_labels[i] = current_num
        old_label     = current_label

    return new_labels, lbl_classes
        
#
# Create a Model 
#
def create_model () :
    ## Convolution Neural Network
    #  2D Convolution   32 Filter, (auf 3x3 Pixel)
    model = tf.keras.models.Sequential()

    model.add( tf.keras.layers.Conv2D( 32, (3, 3), activation = 'relu', input_shape =(128, 128, 3)))  # führt Faltungsoperation mit 32 Filtern über
                                                                                                      # das Bild auf immer 3x3 Pixel durch
    model.add( tf.keras.layers.MaxPooling2D( (2, 2 ) )) # reduziert das Ergebnis des Conv2D layers um den Faktor 2 in jede Richtung
    model.add( tf.keras.layers.Conv2D( 64, (3, 3), activation='relu') )
    model.add( tf.keras.layers.MaxPooling2D((2, 2)))
    model.add( tf.keras.layers.Conv2D( 128, (3, 3), activation='relu') )
    model.add( tf.keras.layers.MaxPooling2D((2, 2)))
    model.add( tf.keras.layers.Conv2D( 128, (3, 3), activation='relu') )
    model.add( tf.keras.layers.Dropout( 0.4 )) # 40% der Ergebnisse werden verworfen
        
    ## Fully Connected Neural Network
    model.add( tf.keras.layers.Flatten() ) # output von Conv2D sind Matrizen in denen räumliche Merkmale festgehalten werden, Flatten() macht 1D Vektoren daraus,
                                           # weil Dense() nur 1d Vektoren erwartet + Verringerung der Berechnungskomplexität
    model.add( tf.keras.layers.Dropout(0.4)) # 40% der Ergebnisse werden verworfen
                                            # vermindert, dass einzelne Ergebnisse nicht überinterpretiert werden               
    model.add( tf.keras.layers.Dense( 256, kernel_regularizer = tf.keras.regularizers.l2(0.007), activation='relu') )
    model.add( tf.keras.layers.Dropout( 0.4 ) )
    model.add( tf.keras.layers.Dense( 128, kernel_regularizer = tf.keras.regularizers.l2(0.007), activation='relu') )
    model.add( tf.keras.layers.Dropout( 0.4 ) )
    model.add( tf.keras.layers.Dense( 64, kernel_regularizer= tf.keras.regularizers.l2(0.007), activation='relu' ) )
    #                        das ergebnis von softmax kann man als Wahrscheinlichkeit interpretieren            
    model.add( tf.keras.layers.Dense( 5, activation = 'softmax' ) )
    
    model.compile( optimizer = 'adam',
                   loss      = tf.keras.losses.SparseCategoricalCrossentropy( from_logits = True ),
                   metrics   = [ 'accuracy' ] )

    return model

#
# split input data in train and test data
#
def split_data ( images, label ) :
    img_train, img_test, labels_train, labels_test = train_test_split( images,
                                                                       labels,
                                                                       test_size    = 0.1, # 10% of images is used as test
                                                                       random_state = 1 )

    return img_train, img_test, labels_train, labels_test

#
# train model by given data
#
def train_model ( model, img_train, img_test, labels_train, labels_test ) :
    labels_test  = np.array( labels_test )
    labels_train = np.array( labels_train )
    img_test     = np.array( img_test )
    img_train    = np.array( img_train )
    
    model.fit( img_train, labels_train, epochs = 50, validation_data = ( img_test, labels_test ) )
    model.evaluate(img_test, labels_test)
    
    model.save( model_path )
    

#
# main function
#

if len( sys.argv ) > 1 :
    dataset_path = sys.argv[1]
    
print( "preprocessing images (%s) ..." % dataset_path )
tic = time()
images, labels = img_preprocessing( dataset_path )
toc = time()
print( "  done in %.3fs" % ( toc - tic ) )

print( "generating labels ..." )
labels, lbl_classes = generate_labels( labels )

print( lbl_classes )

print( "splitting data ..." )
img_train, img_test, lbl_train, lbl_test = split_data( images, labels )

print( "training model ..." )
tic = time()
model = create_model()
train_model( model, img_train, img_test, lbl_train, lbl_test )
toc = time()
print( "  done in %.3fs" % ( toc - tic ) )

print( "finished" )
