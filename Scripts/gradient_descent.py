
samples       = [(0.1, 2.2), (1.06, 3.14), (2.718, 4.4), (3.2, 5.06)]
b             = 0 # theoretisch eine zufällige Zahl
w             = .25
learning_rate = .01


# Mean Squared Error 
def mse(samples, b, w) :
    sum       = 0
    
    for s in samples :
        residual = s[1] - (w * s[0] + b)
        sum += residual**2
    
    sum = sum / len(samples)
    
    return sum
    
# Gradient Descent 
def gradient_descent(samples, b, w) :
    M = float(len(samples))
    b_gradient = 0
    w_gradient = 0
    
    for s in samples :
        b_gradient += (2/M) * ((w * s[0] + b) - s[1])
        w_gradient += (2/M) * s[0] * ((w * s[0] + b) - s[1])
        
    b = b - (b_gradient * learning_rate)
    w = w - (w_gradient * learning_rate)
    
    return b, w 
        
        
def main(samples, b, w) :
    for i in range( 1000 ) :            
        b, w = gradient_descent(samples, b, w)       
        
    print("b : " + str(b) + ", w : " + str(w))

main(samples, b, w)
      
