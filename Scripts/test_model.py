from joblib import load
import os
import cv2

filename        = "/home/paul/faces/Model/model.sav" # Path to save the Model
test_img_folder = "/home/paul/faces"   # Folder with Images for the Prediction
start_label     = ['Emil', 'Nadine', 'Paul', 'Ronald', 'Till']

model = load(filename) # load the model
files = os.listdir(test_img_folder)

for filename in files :    # goes through all images in files and predicts them 
    if filename.endswith(".jpg") :
        
        test_img = cv2.imread(os.path.join(test_img_folder, filename))
        test_img = cv2.resize(test_img, (300, 300))
        
        cv2.imshow("Image for prediction", test_img) # show image 
        
        test_img = cv2.resize(test_img, (100, 100))
        test_img = [test_img.flatten()]

        prediction = model.predict(test_img)
        prediction_label = start_label[int(prediction)]
        
        print("Prediction : " + prediction_label)
        
        while True :
            if cv2.waitKey(1) & 0xFF == ord("n") :
                break

cv2.destroyAllWindows()