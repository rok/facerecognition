import pdb
import numpy as np
import sys

from random import random, seed

def relu(z):
    return z * (z > 0)

def relu_grad(z):
    return 1 * (z > 0)

def sigmoid(z):
    return 1/(1+np.exp(-z))

def sigmoid_grad(z):
    return sigmoid(z) * (1-sigmoid(z))

# to ensure that generated random numbers are the same no matter how many times you run this
np.random.seed(1)
seed( 1 )

#
# define train and test data
#

X_train = []
Y_train = []

for i in range( 1, 1000 ) :
    x = np.zeros( 4 ) # 2x2 field
    y = np.zeros( 2 )

    x[0] = random()
    x[1] = random()
    x[2] = random()
    x[3] = random()

    if x[0] + x[1] > x[2] + x[3] : y[0] = 1
    else                         : y[1] = 1
    
    # p = random()

    # if   p < 0.25 :
    #     x[0] = 1
    #     y[0] = 1
    # elif p < 0.50 :
    #     x[1] = 1
    #     y[0] = 1
    # elif p < 0.75 :
    #     x[2] = 1
    #     y[1] = 1
    # else :
    #     x[3] = 1
    #     y[1] = 1

    X_train.append( x )
    Y_train.append( y )

X = np.array( X_train )
Y = np.array( Y_train )

alpha = 0.01
batch = 1000

hidden_neuron = 2

# make sure the mean of weight is close to zero and std is close to one -- a good practice according to Efficient Backprop paper
w1 = np.random.randn( 4, hidden_neuron )
w2 = np.random.randn( hidden_neuron, 2 )

# backprop + minibatch SGD
for iter in range( batch ):
    error = 0

    for i in range( len(X_train) ):
        x = X[i:i+1]
        y = Y[i:i+1]

        #
        # forward
        #
        
        a0 = x
        
        z1 = np.dot( a0, w1 )
        a1 = sigmoid( z1 )
        
        z2 = np.dot( a1, w2 )
        a2 = sigmoid( z2 )

        #
        # backward
        #
        
        # update w2 first
        err2 = (a2 - y) * sigmoid_grad( z2 )
        w2  -= alpha * np.outer( a1.T, err2)
        
        # update w1
        err1 = np.dot( err2, w2.T ) * sigmoid_grad( z1 )
        w1  -= alpha * np.outer( a0.T, err1 )
        
        error += (np.sum(np.abs(err2)))

    error = error / X.shape[0]

    if iter % 100 == 0 :
        print( "%5d : %.4e" % ( iter, error ) )

print( w1 )
print( w2 )


#
# test
#

x = np.array( [ 0.5,0.1,0.9,0.8 ] )
z00 = ( w1[0,0] * x[0] +
        w1[1,0] * x[1] +
        w1[2,0] * x[2] +
        w1[3,0] * x[3] )
a00 = sigmoid( z00 )

z01 = ( w1[0,1] * x[0] +
        w1[1,1] * x[1] +
        w1[2,1] * x[2] +
        w1[3,1] * x[3] )
a01 = sigmoid( z01 )


z10 = ( w2[0,0] * a00 +
        w2[1,0] * a01 )
a10 = sigmoid( z10 )

z11 = ( w2[0,1] * a00 +
        w2[1,1] * a01 )
a11 = sigmoid( z11 )

print( x, a10, a11 )


for i in range(20) :
    x = X[i]
    y = Y[i]

    z00 = ( w1[0,0] * x[0] +
            w1[1,0] * x[1] +
            w1[2,0] * x[2] +
            w1[3,0] * x[3] )
    a00 = sigmoid( z00 )
    
    z01 = ( w1[0,1] * x[0] +
            w1[1,1] * x[1] +
            w1[2,1] * x[2] +
            w1[3,1] * x[3] )
    a01 = sigmoid( z01 )


    z10 = ( w2[0,0] * a00 +
            w2[1,0] * a01 )
    a10 = sigmoid( z10 )
    
    z11 = ( w2[0,1] * a00 +
            w2[1,1] * a01 )
    a11 = sigmoid( z11 )
    
    print( x, y, a10, a11 )
