import cv2
import os
import shutil

image_folder  = "faces"  # Folder with all Images
target_folder = "faces/Dataset"
image_files   = sorted(os.listdir(image_folder)) # All Files in image_folder

print( "Speicher als erstes die Bilder in Faces ab!!!" )

def main():
    global image_folder
    global image_files

    for image_file in image_files:
        if image_file.endswith(".jpg") :
            image_path = os.path.join(image_folder, image_file)
            
            img = cv2.imread(image_path)
            img = cv2.resize(img, (300, 300))
            cv2.imshow("Image", img)
            
            while True:
                key = cv2.waitKey(0) & 0xFF

                if key == ord("p") :
                    move_image(image_file, "Paul")
                    break
                elif key == ord("e") :
                    move_image(image_file, "Emil")
                    break
                elif key == ord("t") :
                    move_image(image_file, "Till")
                    break
                elif key == ord("n") :
                    move_image(image_file, "Nadine")
                    break
                elif key == ord("r") :
                    move_image(image_file, "Ronald")
                    break
                elif key == ord("d") :
                    os.remove(os.path.join(image_path))
                    break
                elif key == ord("q"):
                    cv2.destroyAllWindows()
                    return

    cv2.destroyAllWindows()

def move_image(img, button): # Move Image
    global image_folder
    global target_folder 
    
    source_path = os.path.join(image_folder, img)  
    target_path = os.path.join(target_folder,  button, img) 
    
    print(target_path)
    
    shutil.move(source_path, target_path)
    

main()
