import cv2
import numpy as np
import pygame
from PIL import Image

pygame.init()

# Filter / Mask
mask = [ 0.0625, 0.125,  0.0625,
         0.125,  0.25, 0.125,
         0.0625, 0.125,  0.0625 ]


n_img  = cv2.imread("/home/paul/facerecognition/conv_img1.png")
img    = cv2.cvtColor(n_img, cv2.COLOR_BGR2RGB)
img    = np.array(img)


width  = n_img.shape[0]
height = n_img.shape[1]
display = pygame.display.set_mode((width, height))
pygame.display.set_caption("Convolution")





def cal_color(colors, mask):
    c = [0, 0, 0]
    for i in range(len(colors)):
        a = colors[i]

        c[0] += colors[i][0] * mask[i]
        c[1] += colors[i][1] * mask[i]
        c[2] += colors[i][2] * mask[i]
    
    c[0] = max(0, min(255, c[0]))
    c[1] = max(0, min(255, c[1]))
    c[2] = max(0, min(255, c[2]))
    
    return c

def convolution( img ) :
    for j in range( n_img.shape[0] ) :
        for i in range( n_img.shape[1] ) :
            color = [[0, 0, 0]] * len(mask)
            
            if j > 0 and i > 0:                  # upper left 
                color[0] = img[j - 1, i - 1]
            if i > 0:                            # upper
                color[1] = img[j, i - 1]
            if j < n_img.shape[0] - 1 and i > 0:  # upper right
                color[2] = img[j + 1, i - 1]
            if j > 0:                            # middle left
                color[3] = img[j - 1, i]
            if j < n_img.shape[0] - 1:           # middle right
                color[5] = img[j + 1, i]
            if j > 0 and i < n_img.shape[1] - 1:  # lower left
                color[6] = img[j - 1, i + 1]
            if i < n_img.shape[1] - 1:           # lower
                color[7] = img[j, i + 1]
            if j < n_img.shape[0] - 1 and i < n_img.shape[1] - 1:  # lower right
                color[8] = img[j + 1, i + 1]

            
            color[4] = img[j, i]
    
            c = cal_color(color, mask)
            display.set_at((j, i), (round(c[0]), round(c[1]), round(c[2])))
            img[j, i] = (round(c[0]), round(c[1]), round(c[2]))
    return img.tolist() 
        

img = convolution( img )

new_img = Image.fromarray(np.uint8(img))
new_img.save("/home/paul/facerecognition/conv_img2.png")

pygame.display.update()













