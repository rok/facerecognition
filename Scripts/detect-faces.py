#!/usr/bin/env python3

import pdb
import sys, os.path
import face_recognition
import cv2
import numpy as np

from   PIL import Image

imfiles = []

if len( sys.argv ) > 1 :
    imfiles = sys.argv[1:]
    
for imfile in imfiles :
    image = cv2.imread(imfile)
    faces = face_recognition.face_locations( image )

    base  = os.path.basename( os.path.splitext( imfile )[0] )
    pimg  = Image.fromarray( image[:, :, ::-1] ) # from BGR to RGB
    
    i = 1
    for ( top, right, bottom, left )  in faces :
        if min( abs(top-bottom), abs(right-left) ) < 150 :
            continue

        # image = cv2.rectangle( image, (left, top),(right, bottom), (0,0,255), 2 )

        face_img = pimg.crop( ( left, top, right, bottom ) )
        face_name = 'face-%s-%d.jpg' % ( base, i )

        if os.path.exists( face_name ) :
            print( "%s already exists" % face_name )
        else :
            face_img.save( 'face-%s-%d.jpg' % ( base, i ) )
        i += 1
