from pandas          import read_csv
from pandas.plotting import scatter_matrix

from matplotlib import pyplot

from sklearn.model_selection       import train_test_split
from sklearn.model_selection       import cross_val_score
from sklearn.model_selection       import StratifiedKFold
from sklearn.metrics               import classification_report
from sklearn.metrics               import confusion_matrix
from sklearn.metrics               import accuracy_score
from sklearn.linear_model          import LogisticRegression
from sklearn.tree                  import DecisionTreeClassifier
from sklearn.neighbors             import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes           import GaussianNB
from sklearn.svm                   import SVC

import joblib

import os
import cv2
import numpy as np

# test_img_folder = "faces"   # Folder with Images for the Prediction
start_label     = ['Emil', 'Nadine', 'Paul', 'Ronald', 'Till'] # all labels
filename        = "/home/paul/faces/Model/model1.sav"   # Path to the saved Model 
dataset         = "/home/paul/faces/Dataset/"  # Folder with the Dataset
                                    # Sequence must stay the same 

#
# Function to preprocess the images and labels
#
def preprocess_images( images ):
    X_processed = []
    for image in images:
        # Flatten the 2D image into a 1D vector -> because the model can only use those
        flattened_image = image.flatten()
        X_processed.append(flattened_image)
    return X_processed


labels = [] # label of each image
images = [] # images 

## Goes through folders, takes all labels from name of folder
#for folder in sorted(os.listdir(dataset)):
#    if folder not in start_label :
#        start_label.append(folder)
        

# Goes through folders, puts images into images and takes label from name 
for folder in sorted(os.listdir(dataset)):
    folder_path = os.path.join(dataset, folder)

    for filename in os.listdir(folder_path):
        if filename.endswith(".jpg") :
            img_path = os.path.join(folder_path, filename)
            
            img = cv2.imread(img_path)
            img = cv2.resize(img, (100, 100))

            # Keep the original three-dimensional structure of the image
            images.append(img)
            labels.append(folder)

print("Image processing completed.")

# Splits the Data into training and validation                                                         
X_train, X_validation, Y_train, Y_validation = train_test_split( images,
                                                                 labels,
                                                                 test_size    = 0.2,    # percent of test data, rest is training data
                                                                 random_state = 1 )     # random number generator which shuffles the training data before assigning them

# Preprocess the images before training the model
X_train_processed = preprocess_images( X_train )

# Convert the labels to numerical values (optional, but some models require it)
label_to_numeric = {label: i for i, label in enumerate(sorted(set(labels)))}
Y_train_numeric  = [label_to_numeric[label] for label in Y_train]


#
#      Checks which model works best
#
#models = []
#models.append(("LR", LogisticRegression(solver="liblinear", multi_class="ovr")))
#models.append(("LDA", LinearDiscriminantAnalysis()))
#models.append(("KNN", KNeighborsClassifier()))
#models.append(("CART", DecisionTreeClassifier()))
#models.append(("NB", GaussianNB()))
#models.append(("SVM", SVC(gamma="auto")))
#
#results = []
#names = []
#
#for name, model in models:
#    kfold = StratifiedKFold(n_splits=10, random_state=1, shuffle=True)
#    cv_results = cross_val_score(model, X_train_processed, Y_train_numeric, cv=kfold, scoring="accuracy")
#    results.append(cv_results)
#    names.append(name)
#    print('%s: %f (%f)' % (name, cv_results.mean(), cv_results.std()))

#
# Train a Model
#
print( "Train model ..." )

model = LogisticRegression( solver      = "sag",    
                            multi_class = "auto" )  
model.fit( X_train_processed, Y_train_numeric )
joblib.dump( model, filename )  # save the model

print("finished")

# #
# # Predict
# #
# print( "Predictions" )

# model = joblib.load(filename) # load the model
# files = os.listdir(test_img_folder)

# for filename in files :    # goes through all images in files and predicts them 
#     if filename.endswith(".jpg") :
        
#         test_img = cv2.imread(os.path.join(test_img_folder, filename))
#         test_img = cv2.resize(test_img, (300, 300))
        
#         cv2.imshow("Image for prediction", test_img) # show image 
        
#         test_img = cv2.resize(test_img, (100, 100))
#         test_img = [test_img.flatten()]

#         prediction = model.predict(test_img)
#         prediction_label = start_label[int(prediction)]
        
#         print("Prediction : " + prediction_label)
        
#         while True :
#             if cv2.waitKey(1) & 0xFF == ord("n") :
#                 break

# cv2.destroyAllWindows()
