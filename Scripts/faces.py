#!/usr/bin/env python3

import pdb

pdb.set_trace()

import os, sys

# disable some messages from TF
os.environ['TF_CPP_MAX_VLOG_LEVEL'] = '-1'

from time import time

import face_recognition
import cv2

import numpy      as np
import tensorflow as tf

from   PIL import Image

model_path   = "Model/test.h5"
model_labels = [ 'Paul', 'Nadine', 'Ronald', 'Emil', 'Till' ]

model = tf.keras.models.load_model( model_path )

imfiles = []

if len( sys.argv ) > 1 :
    imfiles = sys.argv[1:]

#
# return list of detected face positions and corresponding names
#
def recognize ( image ) :
    faces = face_recognition.face_locations( image )

    positions = []
    labels    = []
    for ( top, right, bottom, left ) in faces :
        if min( abs(top-bottom), abs(right-left) ) < 150 :
            continue

        face_img = image[ top:bottom, left:right ]
        face_img = cv2.resize( face_img, ( 128, 128 ) )
        face_img = face_img / 255
        face_img = np.expand_dims( face_img, axis = 0 )

        label_index = np.argmax( model.predict( face_img ), axis = 1 )
        prediction  = model_labels[ label_index[0] ]

        positions.append( ( top, right, bottom, left ) )
        labels.append( prediction )

    return positions, labels
    
for imfile in imfiles :
    image = cv2.imread(imfile)

    poss, lbls = recognize( image )

    for i in range( len(poss) ) :
        ( top, right, bottom, left ) = poss[i]
        cropped = image[ top:bottom, left:right ]
        cv2.imshow( lbls[i], cropped )
        cv2.waitKey()
        print( "%s at ( %d .. %d × %d .. %d )" % ( lbls[i], poss[i][0], poss[i][2], poss[i][1], poss[i][3] ) )
