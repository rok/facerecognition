#!/usr/bin/env python3
#

import pdb
import sys
import math

from random import random

w = [ 1, 2 ]
x = [ 0.25, 0.75 ]
b = 5

w = [ 1.587e+00, -1.173e+00 ]
b = 4.139e+00

# x = 0.25
# y = 1.75
# z = -0.8

# def f ( x, y, z ) :
#     return ( x + y ) * z

# def df ( v, x, y, z ) :
#     return z, z, x + y

# v = f( x, y, z )

# print( "v = ", v )

# dx, dy, dz = df( v, x, y, z )

# print( "dx = ", dx )
# print( "dy = ", dy )
# print( "dz = ", dz )

# h  = 1e-6

# print( "Dx = ", ( f( x+h, y, z ) - v ) / h )
# print( "Dy = ", ( f( x, y+h, z ) - v ) / h )
# print( "Dz = ", ( f( x, y, z+h ) - v ) / h )

def sigmoid ( t ) :
    return 1.0 / ( 1.0 + math.exp( -t ) )

def activation ( t ) :
    return sigmoid( t )

def forward ( x, w, b ) :
    z = w[0] * x[0] + w[1] * x[1] + b
    a = activation( z )
    return a, z
    # l = 0.5 * ( a - 0 )** 2
    # return  a, z, l

def loss ( a, y ) :
    return 0.5 * ( a - y )**2

def backward ( a, z ) :
    # ds = d sigmoid / d z = sigmoid( z )( 1 - sigmoid( z ) ) = a ( 1 - a )
    ds = a * ( 1.0 - a )

    # w = ( ds * x1 , ds * x2 ), b = ds
    return  [ ds * x[0] , ds * x[1] ], ds

#
# some tests
#

a, z = forward( x, w, b )
L    = loss( a, 0 )

print( "z = ", z )
print( "a = ", a )
print( "L = ", L )

w2, b2 = backward( a, z )

print( "dw = ", w2 )
print( "db = ", b2 )

h     = 0.0000001
wtest = [ w[0] + h, w[1] + h ]

atest, ztest = forward( x, [ w[0] + h, w[1] ], b )
print( "Dw0 = ", (atest - a) / h )

atest, ztest = forward( x, [ w[0], w[1] + h ], b )
print( "Dw1 = ", (atest - a) / h )

atest, ztest = forward( x, [ w[0], w[1] ], b + h )
print( "Db  = ", (atest - a) / h )

# sys.exit( 0 )

#
# define train and test data
#

X_train = []
Y_train = []

for i in range( 1, 1000 ) :
    p = random()
    x = [ p, 1.0 - p ]
    y = 0
    if p > 0.5 : y = 1 
    X_train.append( x )
    Y_train.append( y )

X_test = []
Y_test = []

for i in range( 1, 1000 ) :
    p = random()
    x = [ p, 1.0 - p ]
    y = 0
    if p > 0.5 : y = 1

    x = [ random(), random() ]
    y = 0
    if x[0] > x[1] : y = 1 
    
    X_test.append( x )
    Y_test.append( y )

#
# train
#

eta = 0.1

w = [ 0, 0 ]
b = 0

M = len(X_train)

for epoch in range( 1000 ) :
    dw0 = 0
    dw1 = 0
    db  = 0

    loss = 0
    for i in range( M ) :
        x_i = X_train[i]
        y_i = Y_train[i]

        o_i, z_i = forward( x_i, w, b )
        l_i      = (o_i - y_i)
        loss    += l_i * l_i
        
        dw0 += l_i * o_i * ( 1.0 - o_i ) * x_i[0]
        dw1 += l_i * o_i * ( 1.0 - o_i ) * x_i[1]
        db  += l_i * o_i * ( 1.0 - o_i )

    w = [ w[0] - eta * dw0,
          w[1] - eta * dw1 ]
    b = b - eta * db

    loss = loss / M
    # for x in X_train :
    #     print( x, forward( x, w, b )[0] )
    
    # w = [ w[0] - eta * dw0,
    #       w[1] - eta * dw1 ]
    # b = b - eta * db

    if epoch % 1000 == 0 :
        print( "%4d : loss = %.3e, w = %.3e / %.3e, b = %.3e" % ( epoch, loss, w[0], w[1], b ) )

# w = 2.521e+02 / -2.521e+02, b = 2.206e-02
# w = [ 100, -100 ]
# b = 0.3

# for i in range(len(X_test)) :
for i in range(10) :
    x_i = ( random(), random() ) # X_test[i]
    y_i = 0
    if x[0] > x[1] :
        y_i = 1
    # y_i = Y_test[i]
    print( "%.1f / %.1f : %4.0f   %.1f" % ( x_i[0], x_i[1], y_i, forward( x_i, w, b )[0] ) )
