import tensorflow as tf
import numpy      as np
import cv2
import os

img_path   = "/home/paul/faces/face-WhatsApp Image 2023-09-02 at 19.57.43-1.jpg"
model_path = "/home/paul/faces/Model/test.h5"
label      = ['Paul', 'Nadine', 'Ronald', 'Emil', 'Till']

img = cv2.imread( img_path )
img = cv2.resize( img, ( 128, 128 ) )
img = img / 255
img = np.expand_dims( img, axis = 0 )

model = tf.keras.models.load_model(model_path)

label_index = np.argmax(model.predict(img), axis=1)
prediction  = label[ label_index[0] ]
print(prediction)
