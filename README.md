Face Recognition
================

## detect-faces.py

Extracts all faces from a given image and saves them in the local directory as 
`face-<imgname>-n.jpg` where *imgname* is the original image file name and *n* the n'th
detected face.

## test.py

???

## train_model.py

???

## sort_faces.py

Goes through all detected faces and sorts (labels) them into a given set of subdirectories
(which face belongs to which person).
