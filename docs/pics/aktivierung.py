#!/usr/bin/env python3

import numpy as np
import math

import matplotlib as mpl
import matplotlib.pylab as pl

from config import *

mpl.use('Agg')
mpl.rcParams.update( theme  )
mpl.rcParams.update( { 'figure.figsize' : ( 8, 7 ) } )

import matplotlib.pyplot               as     plt
from   matplotlib.backends.backend_pdf import PdfPages

#
# Definition Funktionen
#

def sigmoid ( x ) :
    return 1.0 / ( 1.0 + np.exp( -x ) )

def sigmoid_dx ( x ) :
    return sigmoid(x) * ( 1.0 - sigmoid( x ) )

def relu ( x ) :
    return ( x + np.abs(x) ) / 2.0

def relu_dx ( x ) :
    return x > 0

X  = np.linspace( -10, 10, 1000 )

###########################################################
#
# plotte sigmoid
#
###########################################################

plt.figure()
ax = plt.subplot( 1, 1, 1 ) 

ax.set_ylabel( 'y' )
ax.set_xlabel( 'x' )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.axhline( y = 0, color = 'k' )
ax.axvline( x = 0, color = 'k' )

ax.plot( X, sigmoid(X), label = 'f',  color = colors[1], linestyle = 'solid', lw = 5 )

plt.tight_layout()

pp = PdfPages( 'sigmoid.pdf' )
pp.savefig()
pp.close()
plt.close()

plt.figure()
ax = plt.subplot( 1, 1, 1 ) 

ax.set_ylabel( 'y' )
ax.set_xlabel( 'x' )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.axhline( y = 0, color = 'k' )
ax.axvline( x = 0, color = 'k' )

# ax.plot( X, sigmoid(X),    label = '$\sigma(x)$',  color = colors[1], linestyle = 'solid', lw = 5 )
ax.plot( X, sigmoid_dx(X), label = '$d \sigma(x) / dx$',  color = colors[2], linestyle = 'solid', lw = 5 )

# ax.legend( loc = "upper left", ncol = 1 )

plt.tight_layout()

pp = PdfPages( 'sigmoid2.pdf' )
pp.savefig()
pp.close()
plt.close()

###########################################################
#
# plotte Funktion und Tangente
#
###########################################################

plt.figure()
ax = plt.subplot( 1, 1, 1 ) 

ax.set_ylabel( 'y' )
ax.set_xlabel( 'x' )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.axhline( y = 0, color = 'k' )
ax.axvline( x = 0, color = 'k' )

ax.plot( X, relu(X), label = 'f',  color = colors[1], linestyle = 'solid', lw = 5 )

plt.tight_layout()

pp = PdfPages( 'relu.pdf' )
pp.savefig()
pp.close()
plt.close()

plt.figure()
ax = plt.subplot( 1, 1, 1 ) 

ax.set_ylabel( 'y' )
ax.set_xlabel( 'x' )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.axhline( y = 0, color = 'k' )
ax.axvline( x = 0, color = 'k' )

# ax.plot( X, relu(X),    label = 'ReLu(x)',  color = colors[1], linestyle = 'solid', lw = 5 )
ax.plot( X, relu_dx(X), label = 'd ReLu(x) / dx',  color = colors[2], linestyle = 'solid', lw = 5 )

plt.tight_layout()

pp = PdfPages( 'relu2.pdf' )
pp.savefig()
pp.close()
plt.close()

