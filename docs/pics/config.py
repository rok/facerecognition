theme = { 'text.usetex'           : False,
          'font.family'           : 'Roboto Condensed',
          #'figure.facecolor'      : "#fdfdf6",
          'axes.titlesize'        : 20,
          'axes.titlelocation'    : 'center',
          'axes.labelsize'        : 20,
          'axes.labelcolor'       : "#000000",
          #'axes.facecolor'        : "#fdfdf6",
          'lines.markeredgewidth' : 0.0,
          'lines.markersize'      : 10,
          'lines.markeredgecolor' : 'none',
          'lines.linewidth'       : 3,
          'legend.fontsize'       : 16,
          'legend.framealpha'     : 1.0,
          'legend.frameon'        : True,
          'legend.fancybox'       : True,
          'legend.borderpad'      : 0.4,
          'legend.handlelength'   : 2.5,
          'xtick.labelsize'       : 18,
          'ytick.labelsize'       : 18,
          'figure.figsize'        : ( 7, 7 )
         }

colors = [ '#000000', # black
           '#cc0000', # scarletred2
           '#3465a4', # skyblue2
           '#4e9a06', # chameleon3
           '#f57900', # orange2
           '#75507b', # plum2
           '#c17d11', # chocolate2
           '#edd400', # butter2
           '#83a598', # faded aqua
          ]

line_styles = [ 'solid',
                'dashed',
                'dotted',
                'dashdot',
                (0,(3,1,1,1)) ]
