#!/usr/bin/env python3

import numpy as np
import math

import matplotlib as mpl

from config import *

mpl.use('Agg')
mpl.rcParams.update( theme  )

import matplotlib.pyplot               as     plt
from   matplotlib.backends.backend_pdf import PdfPages

#
# Definition Funktion und Ableitung
#

def f ( x ) :
    # return x**2*np.sin(x)
    return 0.01*(x**3 - 10*x**2 + 100)

def df ( x ) :
    # return 2*x*np.sin(x) + x**2*np.cos(x)
    return 0.01*(3*x**2 - 20*x)

X  = np.linspace( -5, 12, 1000 )
F  = f( X )
dF = df( X )

###########################################################
#
# plotte Funktion
#
###########################################################

plt.figure()
ax = plt.subplot( 1, 1, 1 ) 

ax.set_ylabel( 'y' )
ax.set_xlabel( 'x' )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.set_xlim( -5, 12 )
ax.set_ylim( -3, 4 )

ax.axhline( y = 0, color = 'k' )
ax.axvline( x = 0, color = 'k' )

ax.plot( X,  F, label = 'f',  color = colors[1], linestyle = 'solid', lw = 3 )

ax.legend( loc = "lower right", ncol = 1 )

plt.tight_layout()

pp = PdfPages( 'funktion.pdf' )
pp.savefig()
pp.close()

###########################################################
#
# plotte Funktion und Tangente
#
###########################################################

plt.figure()
ax = plt.subplot( 1, 1, 1 ) 

ax.set_ylabel( 'y' )
ax.set_xlabel( 'x' )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.set_xlim( -5, 12 )
ax.set_ylim( -3, 4 )

ax.axhline( y = 0, color = 'k' )
ax.axvline( x = 0, color = 'k' )

ax.plot( X,  F, label = 'f',  color = colors[1], linestyle = 'solid', lw = 3 )

#
# male Tangenten an f

h = 3

def tangente ( x ) :
    ax.plot( [ x - h, x, x + h ],
             [ f(x) - h * df(x), f(x), f(x) + h * df(x) ],
             color = colors[6], lw = 2, ls = 'dashed' )

tangente( -1.0 )
tangente(  6.0 )

ax.legend( loc = "lower right", ncol = 1 )

plt.tight_layout()

pp = PdfPages( 'tangente.pdf' )
pp.savefig()
pp.close()
plt.close()


#
# Tangenten einzeln
#

i = 1
for x in [ -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ] :
    plt.figure()
    ax = plt.subplot( 1, 1, 1 ) 
    
    ax.set_ylabel( 'y' )
    ax.set_xlabel( 'x' )
    ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
    ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )
    
    ax.set_xlim( -5, 12 )
    ax.set_ylim( -3, 4 )

    ax.axhline( y = 0, color = 'k' )
    ax.axvline( x = 0, color = 'k' )
    
    ax.plot( X,  F, label = 'f',  color = colors[1], linestyle = 'solid', lw = 3 )
    
    #
    # male Tangenten an f
    
    h = 3
    
    tangente( x )
    ax.plot( [ x, x ],  [ 0, f(x) ], color = 'k', linestyle = 'solid', lw = 3 )

    ax.legend( loc = "lower right", ncol = 1 )

    plt.tight_layout()
    
    pp = PdfPages( 'tangente-%02d.pdf' % i )
    pp.savefig()
    pp.close()
    plt.close()
    i += 1

#
# Tangenten einzeln
#

i = 1
for x in [ 1, 2, 3, 4, 5, 5.5, 6, 6.25, 6.3, 6.35, 6.45, 6.5, 6.55, 6.6, 6.65 ] :
    plt.figure()
    ax = plt.subplot( 1, 1, 1 ) 
    
    ax.set_ylabel( 'y' )
    ax.set_xlabel( 'x' )
    ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
    ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )
    
    ax.set_xlim( -5, 12 )
    ax.set_ylim( -3, 4 )
    
    ax.axhline( y = 0, color = 'k' )
    ax.axvline( x = 0, color = 'k' )
    
    ax.plot( X,  F, label = 'f',  color = colors[1], linestyle = 'solid', lw = 3 )
    # ax.plot( X, dF, label = "$f\'$", color = colors[2], linestyle = 'solid', lw = 3 )
    
    #
    # male Tangenten an f
    
    h = 3
    
    tangente( x )
    ax.plot( [ x, x ],  [ 0, f(x) ], color = 'k', linestyle = 'solid', lw = 3 )

    ax.legend( loc = "lower right", ncol = 1 )

    plt.tight_layout()
    
    pp = PdfPages( 'tangente-ableitung-%02d.pdf' % i )
    pp.savefig()
    pp.close()
    plt.close()
    i += 1

###########################################################
#
# plotte Funktion und Ableitung
#
###########################################################

plt.figure()
ax = plt.subplot( 1, 1, 1 ) 

ax.set_ylabel( 'y' )
ax.set_xlabel( 'x' )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.set_xlim( -5, 12 )
ax.set_ylim( -3, 4 )

ax.axhline( y = 0, color = 'k' )
ax.axvline( x = 0, color = 'k' )

ax.plot( X,  F, label = 'f',  color = colors[1], linestyle = 'solid', lw = 3 )
ax.plot( X, dF, label = "$f\'$", color = colors[2], linestyle = 'solid', lw = 3 )

ax.legend( loc = "lower right", ncol = 1 )

plt.tight_layout()

pp = PdfPages( 'ableitung.pdf' )
pp.savefig()
pp.close()


###########################################################
#
# plotte Funktion mit Minimum
#
###########################################################

plt.figure()
ax = plt.subplot( 1, 1, 1 ) 

ax.set_ylabel( 'y' )
ax.set_xlabel( 'x' )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.set_xlim( -5, 12 )
ax.set_ylim( -3, 4 )

ax.axhline( y = 0, color = 'k' )
ax.axvline( x = 0, color = 'k' )

ax.plot( X,  F, label = 'f',  color = colors[1], linestyle = 'solid', lw = 3 )

ax.annotate( 'Minimum', xy = ( 6.65, -0.5 ), xytext = (8, -2),
             arrowprops = dict( facecolor = 'black', arrowstyle = 'simple' ),
             fontsize   = 16 )

ax.legend( loc = "lower right", ncol = 1 )

plt.tight_layout()

pp = PdfPages( 'funktion-min.pdf' )
pp.savefig()
pp.close()

