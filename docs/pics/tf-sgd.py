import matplotlib as mpl

from config import *

mpl.use('Agg')
mpl.rcParams.update( theme  )
mpl.rcParams.update( { 'figure.figsize' : ( 8, 7 ) } )

import matplotlib.pyplot as plt
import sgd
import sgd_mse

mse_loss = sgd_mse.loss
mse_acy  = sgd_mse.accuracy

cce_loss = sgd.loss
cce_acy = sgd.accuracy

epochs = 100

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_title( 'Genauigkeit' )
ax.set_ylim([0, 1.05])
ax.set_xlim([0, 100])

ax.plot( range(0, epochs), mse_acy, label="MSE", color = colors[1], linestyle = 'solid', lw = 4 )
ax.plot( range(0, epochs), cce_acy, label="CCE", color = colors[2], linestyle = 'dashed', lw = 4 )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )
    
ax.set_xlabel("Epochs")
    
ax.legend( loc ='center right', ncol = 1 )

plt.tight_layout()
plt.savefig( 'tf-sgd1.pdf' )
plt.close()


fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_title( 'Loss' )
ax.set_xlim([0, 100])
ax.set_ylim([1e-5, 10])
ax.set_yscale( 'log', base = 10 )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.plot(range(0, epochs), mse_loss, label="MSE", color = colors[1], linestyle = 'solid', lw = 4 )
ax.plot(range(0, epochs), cce_loss, label="CCE", color = colors[2], linestyle = 'dashed', lw = 4 )
    
ax.set_xlabel("Epochs")
    
ax.legend( loc ='center right', ncol = 1 )

plt.tight_layout()
plt.savefig( 'tf-sgd2.pdf' )
plt.close()
