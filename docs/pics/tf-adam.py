import matplotlib as mpl

from config import *

mpl.use('Agg')
mpl.rcParams.update( theme  )
mpl.rcParams.update( { 'figure.figsize' : ( 8, 7 ) } )

import matplotlib.pyplot as plt
import adam
import sgd

sgd_loss = sgd.loss
sgd_acy  = sgd.accuracy

adam_loss = adam.loss
adam_acy = adam.accuracy

epochs = 100

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_title( 'Genauigkeit' )
ax.set_xlabel("Epochs")
ax.set_ylim([0, 1.05])
ax.set_xlim([0, 100])
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.plot(range(0, epochs), sgd_acy,  label="SGD",  color = colors[1], linestyle = 'solid', lw = 4 )
ax.plot(range(0, epochs), adam_acy, label="Adam", color = colors[2], linestyle = 'dashed', lw = 4 )

ax.legend( loc = 'lower right' )
    
plt.tight_layout()
plt.savefig( 'tf-adam1.pdf' )
plt.close()



fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_title("Loss")
ax.set_xlabel("Epochs")
ax.set_xlim([0, 100])
ax.set_ylim([1e-7, 5])
ax.set_yscale( 'log', base = 10 )
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.plot(range(0, epochs), sgd_loss, label="Loss (SGD)",   color = colors[1], linestyle = 'solid', lw = 4 )
ax.plot(range(0, epochs), adam_loss, label="Loss (Adam)", color = colors[2], linestyle = 'dashed', lw = 4 )

ax.legend( loc = 'upper right' )
    
plt.tight_layout()
plt.savefig( 'tf-adam2.pdf' )
plt.close()


