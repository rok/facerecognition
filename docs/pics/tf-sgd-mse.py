import matplotlib as mpl

from config import *

mpl.use('Agg')
mpl.rcParams.update( theme  )
mpl.rcParams.update( { 'figure.figsize' : ( 8, 7 ) } )

import matplotlib.pyplot as plt
import sgd_mse

loss = sgd_mse.loss
acy  = sgd_mse.accuracy
epochs = 100

fig = plt.figure()
    
ax = fig.add_subplot(111)
ax.set_title( "Genauigkeit" )
ax.plot( range(0, epochs), acy )
ax.set_ylim([0, 1])
ax.set_xlim([0, 100])
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.set_xlabel( "Epochs" )

ax.plot( range(0, epochs), acy, color = colors[1], lw = 4 )

plt.tight_layout()
plt.savefig( 'tf-sgd-mse1.pdf' )
plt.close()

fig = plt.figure()
    
ax = fig.add_subplot(111)
ax.set_title( "Loss" )
ax.set_xlim([0, 100])
ax.set_ylim([0, 10])
ax.grid( which = 'major', linestyle = '-', linewidth='0.5',  color='#888a85' )
ax.grid( which = 'minor', linestyle = ':', linewidth='0.25', color='#d3d7cf' )

ax.plot(range(0, epochs), loss, color = colors[2], lw = 4 )

ax.set_xlabel( "Epochs" )
# ax.set_ylabel( "Loss" )

plt.tight_layout()
plt.savefig( 'tf-sgd-mse2.pdf' )
plt.close()

