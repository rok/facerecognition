#!/usr/bin/env python3

import pdb

import numpy as np
import math

import matplotlib as mpl
import matplotlib.pylab as pl

from config import *

mpl.use('Agg')
mpl.rcParams.update( theme  )

import matplotlib.pyplot               as     plt
from   matplotlib.backends.backend_pdf import PdfPages
from   mpl_toolkits.mplot3d            import Axes3D

#
# Definition Funktion und Ableitung
#

cx = 1.0
cy = 1.0

def f ( x, y ) :
    return x*x + y*y
    return -( np.sin( cx * x ) + np.sin( cy * y ) )
    return ( x**2 + y - 11 )**2 + ( x + y**2 - 7)**2
    return - ( np.cos(x)**2 + np.cos(y)**2 )**2

def df_x ( x ) :
    return 2*x
    return cx * np.cos( cx * x )
    return 4 * ( x**2 + y - 11 ) * x + 2 * ( x + y**2 - 7 )
    return 4 * ( np.cos(x)**2 + np.cos(y)**2 )**2 * np.cos(x) * np.sin(x)

def df_y ( y ) :
    return 2*y
    return cy * np.cos( cy * y )
    return 2 * ( x**2 + y - 11 ) + 4 * ( x + y**2 - 7 ) * y
    return 4 * ( np.cos(x)**2 + np.cos(y)**2 )**2 * np.cos(y) * np.sin(y)

N = 100

minx = -1
maxx =  1
miny = -1
maxy =  1

x = np.linspace( minx, maxx, N )
y = np.linspace( miny, maxy, N )

X, Y = np.meshgrid( x, y, sparse = False, indexing = 'ij' )
F    = f( X, Y )

# print( F )

###########################################################
#
# plotte Funktion und Tangente
#
###########################################################

fig = plt.figure()
ax  = fig.add_subplot( projection = '3d' )
ax.set_xlim( minx, maxx )
ax.set_ylim( miny, maxy )

minz = np.min( F )
maxz = np.max( F )

ax.plot_surface( X, Y, F, cmap = 'viridis', alpha = 0.5 )
ax.plot_wireframe( X, Y, F, colors = '#808080', linewidths = 0.1 )

#
# gradient field
#

N = 15
x = np.linspace( minx, maxx, N )
y = np.linspace( miny, maxy, N )
X, Y = np.meshgrid( x, y, sparse = False, indexing = 'ij' )
dF_x = df_x( X )
dF_y = df_y( Y )

# norms (for colours)
C = np.zeros( [ N * N ] )

for i in range(N) :
    for j in range(N) :
        C[i+j*N] = math.sqrt( dF_x[i,j]*dF_x[i,j] + dF_y[i,j]*dF_y[i,j] )

M = max( C )
m = min( C )

C = [ c / M for c in C ]
C = np.concatenate( ( C, np.repeat( C, 2 ) ) )
C = plt.cm.coolwarm( C )

ax.quiver( X,    Y,    [ minz for i in X ],
           dF_x, dF_y, [    0 for i in X ],
#            colors    = C,
           length    = 0.1,
           linewidth = 1.5,
           cmap      = 'viridis' )

ax.set_xticks( np.linspace( minx, maxx, 10 ), [ '' for x in np.linspace( minx, maxx, 10 ) ] )
ax.set_yticks( np.linspace( miny, maxy, 10 ), [ '' for x in np.linspace( miny, maxy, 10 ) ] )
ax.set_zticks( np.linspace( minz, maxz, 10 ), [ '' for x in np.linspace( minz, maxz, 10 ) ] )

plt.tight_layout()
pp = PdfPages( "gradient.pdf" )
pp.savefig()
pp.close()
plt.close()

