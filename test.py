from sklearn.model_selection import train_test_split
from tensorflow.keras import layers, models

import tensorflow as tf
import numpy      as np
import cv2
import os 

test_img_folder = "/home/paul/faces/"  
start_label     = []
filename        = "/home/paul/faces/Models/test_model.h5"
dataset         = "/home/paul/faces/Dataset"


#
# prepare image for actual use (2D -> 1D)
#
def preprocess_images ( images ) :
    X_processed = []
    for image in images:
        # Flatten the 2D image into a 1D vector -> because the model can only use those
        resized_image   = cv2.resize(image, (100, 100))
        flattened_image = resized_image.flatten()
        X_processed.append(flattened_image)
    return X_processed

labels = []  # labels of each image
images = []  # images

# Goes through folders, takes all labels from name of folder
for folder in sorted( os.listdir( dataset ) ):
    if folder not in start_label:
        start_label.append(folder)

# Goes through folders, puts images into images and takes label from name
for folder in sorted( os.listdir( dataset ) ):
    folder_path = os.path.join(dataset, folder)

    for filename in os.listdir(folder_path):
        if filename.endswith(".jpg"):
            img_path = os.path.join(folder_path, filename)

            img = cv2.imread(img_path)
            images.append(img)
            labels.append(folder)

print( "Image processing completed." )

# Check if all images have the same shape and change them if needed
max_shape = max( [ image.shape for image in images ] )
for i in range(len(images)):
    if images[i].shape != max_shape:
        images[i] = cv2.resize(images[i], (max_shape[1], max_shape[0]))

# Convert data to NumPy arrays
X = np.array( images )
Y = np.array( labels )

# Convert the labels to numerical values (optional, but some models require it)
label_to_numeric = { label: i for i, label in enumerate(sorted(set(Y))) }
Y_numeric        = np.array([label_to_numeric[label] for label in Y])

# Splits the Data into training and validation
X_train, X_validation, Y_train, Y_validation = train_test_split( X, Y_numeric,
                                                                 test_size    = 0.1,
                                                                 random_state = 1 )

# preprocess images
X_train_processed      = preprocess_images( X_train )
X_validation_processed = preprocess_images( X_validation )
print("Preprocessed Images")


print("Train model...")
# Create CNN-Model 
model = models.Sequential()

model.add( layers.Conv2D( 32, (3, 3), activation = 'relu', input_shape = ( 100, 100, 3) ) )
model.add( layers.MaxPooling2D( (2, 2) ) )
model.add( layers.Conv2D( 64, (3, 3), activation = 'relu'))
model.add( layers.MaxPooling2D((2, 2)))
model.add( layers.Conv2D( 128, (3, 3), activation = 'relu'))
model.add( layers.MaxPooling2D( (2, 2) ) )
model.add( layers.Flatten() )
model.add( layers.Dense( 128, activation = 'relu' ) )
model.add( layers.Dense( len(start_label), activation = 'softmax' ) )

# Compile model
model.compile( optimizer = 'adam',
               loss      = 'sparse_categorical_crossentropy',
               metrics   = ['accuracy'] )

# train model
model.fit( np.array( X_train_processed ), Y_train,
           epochs     = 10,
           batch_size = 32 )

# evaluate model
test_loss, test_accuracy = model.evaluate( np.array( X_validation_processed ), Y_validation )
print( 'Test Accuracy:', test_accuracy )

# save model
model.save( filename )
print( "Modell saved." )

# Prediction
loaded_model = models.load_model( filename )

files = os.listdir( test_img_folder )

for filename in files:
    if filename.endswith(".jpg"):
        test_img = cv2.imread(os.path.join(test_img_folder, filename))
        test_img = cv2.resize(test_img, (100, 100))
        test_img = np.array(test_img).reshape(1, 100, 100, 3)

        prediction            = loaded_model.predict( test_img )
        predicted_label_index = np.argmax( prediction)
        predicted_label       = start_label[ predicted_label_index ]

        print( "Prediction : " + str(predicted_label) )

        while True : 
            if cv2.waitKey(0) & 0xFF == ord("n") : 
                break
